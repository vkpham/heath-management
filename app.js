const eroc = require('eroc')

const router = require('./router')


const app = eroc.createApplication((app) => {
    app.use('/api/user', router)
})

app.start()

require('./boot')()
require('./kafka')